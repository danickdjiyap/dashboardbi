# DashboardBI

DashboardBI est une petite application web présentant statisquement les informations stockées dans la base de donnée.

## Environnement de développement

### Pré-requis

* PHP 8.0.2
* Composer
* laravel CLI
* nodejs & npm

### Lancer l'environement de développement

```bash
composer install
npm install
npm run build
php artisan serve
```
