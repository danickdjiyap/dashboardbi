<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('periodeOfPub');
            $table->string('JournalImpactFactor')->nullable();
            $table->string('journalRank')->nullable();
            $table->string('numberOfAuthor');
            $table->string('citedBy')->nullable();
            $table->string('isbn')->nullable();
            $table->string('issn')->nullable();
            $table->string('publisher');
            $table->text('abstract');
            $table->text('link');
            $table->string('doi')->nullable();
            $table->text('title');
            $table->text('sourceTile');
            $table->text('cleanTitle');
            $table->string('publicationYear');
            $table->text('authorsID');
            $table->text('authors');
            $table->string('firstAuthorScopusID');
            $table->string('nbCortaCoauthPaper');
            $table->string('thePaperHasCortaFelAsCoauth');
            $table->string('theCortaFelIsTheFirstAuthOfThis');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
};
