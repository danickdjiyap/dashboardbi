<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auteurs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('uniqueID');
            $table->string('currentCity');
            $table->string('currentCountry');/*
            $table->integer('nbPubBeforeCorta')->nullable();
            $table->integer('nbPubDuringCorta')->nullable();
            $table->integer('nbPubAfterCorta')->nullable();*/
            $table->string('scopusID');
            $table->string('docMCperTenPubFel')->nullable();
            $table->string('PTNDocMCOfTheTPub');
            $table->string('TNDocMCPerFellow')->nullable();
            $table->string('gender');
            $table->string('cohort');
            $table->string('startYear')->nullable();
            $table->string('nationality');
            $table->string('homeInstitution');
            $table->string('hostInstitution')->nullable();
            $table->string('alternativeEmail')->nullable();
            $table->string('email');
            $table->string('yearOfCompleting')->nullable();
            $table->string('currentPHPStatus');
            $table->string('totalCo-authors');
            $table->string('h-index')->nullable();
            $table->string('numberOfCitation')->nullable();
            $table->string('numberOfPublication')->nullable();
            $table->string('scopusNumber');
            $table->string('nbCitBeforeCorta');
            $table->string('nbCitDuringCorta');
            $table->string('nbCitAfterCorta');
            $table->string('mCTcombined1')->nullable();
            $table->string('nbDocMC1')->nullable();
            $table->string('mCTcombined2')->nullable();
            $table->string('nbDocMC2')->nullable();
            $table->string('mCTcombined3')->nullable();
            $table->string('nbDocMC3')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auteurs');
    }
};
