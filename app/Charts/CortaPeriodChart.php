<?php

namespace App\Charts;

use Illuminate\Support\Facades\DB;
use ArielMejiaDev\LarapexCharts\DonutChart;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class CortaPeriodChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): DonutChart
    {
        $data = DB::select(DB::raw("select count(*) as total from articles group by periodeOfPub"));
        $chardata = [];
        foreach ($data as $k) {
            array_push($chardata, $k->total);
        }
        return $this->chart->donutChart()
            ->setTitle('Publications per corta period.')
            ->setSubtitle('Showing percentage of publication based on carta period.')
            ->addData($chardata)
            ->setLabels(['During', 'Before', 'After']);
    }
}
