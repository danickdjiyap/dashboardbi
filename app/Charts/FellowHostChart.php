<?php

namespace App\Charts;

use Illuminate\Support\Facades\DB;
use ArielMejiaDev\LarapexCharts\PieChart;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class FellowHostChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): PieChart
    {
        $data = DB::select(DB::raw("SELECT COUNT(*) as total, hostInstitution FROM `auteurs` WHERE hostInstitution is NOT null GROUP by hostInstitution"));
        
        $chardatav = [];
        $chardatak = [];
        foreach ($data as $k) {
            array_push($chardatav, $k->total);
            array_push($chardatak, $k->hostInstitution);
        }
        return $this->chart->pieChart()
        ->setTitle('Fellows per host institut.')
            ->addData($chardatav)
            ->setLabels($chardatak);
    }
}
