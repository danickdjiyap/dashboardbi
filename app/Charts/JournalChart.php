<?php

namespace App\Charts;

use Illuminate\Support\Facades\DB;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use ArielMejiaDev\LarapexCharts\PolarAreaChart;

class JournalChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): PolarAreaChart
    {
        $data = DB::select(DB::raw("select count(*) as total, sourceTile from articles group by sourceTile order by total desc LIMIT 5"));
        
        $chardatav = [];
        $chardatak = [];
        foreach ($data as $k) {
            array_push($chardatav, $k->total);
            array_push($chardatak, $k->sourceTile);
        }
   
        return $this->chart
            ->polarAreaChart()
            ->setTitle('Top 5 journals.')
            ->setSubtitle('Global corta top 5 journals.')
            ->addData($chardatav)
            ->setLabels($chardatak);
    }
}
