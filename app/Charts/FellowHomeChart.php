<?php

namespace App\Charts;

use Illuminate\Support\Facades\DB;
use ArielMejiaDev\LarapexCharts\PieChart;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class FellowHomeChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): PieChart
    {
        $data = DB::select(DB::raw("select count(*) as total, homeInstitution from auteurs group by homeInstitution"));
        
        $chardatav = [];
        $chardatak = [];
        foreach ($data as $k) {
            array_push($chardatav, $k->total);
            array_push($chardatak, $k->homeInstitution);
        }
        return $this->chart->pieChart()
            ->setTitle('Fellows per home institut.')
            ->addData($chardatav)
            ->setLabels($chardatak);
    }
}
