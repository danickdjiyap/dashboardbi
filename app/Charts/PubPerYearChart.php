<?php

namespace App\Charts;

use Illuminate\Support\Facades\DB;
use ArielMejiaDev\LarapexCharts\BarChart;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class PubPerYearChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): BarChart
    {
        $date = DB::select(DB::raw("SELECT COUNT(*) as total, publicationYear FROM `articles` WHERE publicationYear > 2010 GROUP by publicationYear"));
        
        
        $chardate = [];
        $charduring = [];
        $charbefore = [];
        $charafter = [];
        foreach ($date as $k) {
            array_push($chardate, $k->publicationYear);
            $data = DB::select(DB::raw("SELECT COUNT(*) as total, periodeOfPub FROM `articles` WHERE publicationYear = ".$k->publicationYear." GROUP by `periodeOfPub`"));
            $testb=0;
            $testd=0;
            $testa=0;
            foreach ($data as $key) {
                if ($key->periodeOfPub == "Before") {
                    array_push($charbefore, $key->total);
                    $testb++;
                }
                if ($key->periodeOfPub == "During") {
                    array_push($charduring, $key->total);
                    $testd++;
                }
                if ($key->periodeOfPub == "After") {
                    array_push($charafter, $key->total);
                    $testa++;
                }
                
            }
            if ($testb == 0){
                array_push($charbefore, 0);
            }
            if ($testd == 0){
                array_push($charduring, 0);
            }
            if ($testa == 0){
                array_push($charafter, 0);
            }
        }
        return $this->chart->barChart()
            ->setTitle('Publication per year.')
            ->setSubtitle('Evolution of corta publication period (Before, During an After).')
            ->addData('Before', $charbefore)
            ->addData('During', $charduring)
            ->addData('After', $charafter)
            ->setXAxis($chardate);
    }
}
