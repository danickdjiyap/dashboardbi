<?php

namespace App\Http\Controllers\Admin;

use App\Models\Auteur;
use App\Models\Article;
use App\Models\Citation;
use App\Charts\JournalChart;
use Illuminate\Http\Request;
use App\Charts\FellowHomeChart;
use App\Charts\FellowHostChart;
use App\Charts\PubPerYearChart;
use App\Charts\CortaPeriodChart;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(JournalChart $chart, CortaPeriodChart $cortaChart, FellowHostChart $fellowHostChart, FellowHomeChart $fellowHomeChart, PubPerYearChart $pubPerYearChart)
    {

        $factors = DB::select(DB::raw("SELECT AVG(journalImpactFactor) as factor FROM articles"));
        foreach ($factors as $k){
            $factor = $k->factor;
        }

        $nbpub = Article::count();
        $nbcitations = Citation::count();
        $nbauteurs = DB::select(DB::raw("SELECT count(*) AS total_auteur FROM auteurs GROUP BY uniqueID"));
        $total_auteur = 0;
        foreach ($nbauteurs as $k) {
            $total_auteur++;
        }

        $firtAuthFel = Article::where('theCortaFelIsTheFirstAuthOfThis', 'Yes')->count();
        $noname = DB::select(DB::raw("SELECT COUNT(publisher) as total, publisher FROM articles GROUP BY publisher LIMIT 1"));
        foreach ($noname as $k) {
            $nbnoname = $k->total;
        }
        $online = DB::select(DB::raw("SELECT count(*) AS total FROM articles WHERE publisher IS NOT null"));
        foreach ($online as $k) {
            $online = $k->total;
        }
        $online = $online - $nbnoname;
        $six = ((100 * $firtAuthFel) / ($nbpub));

        $six = round($six, 2);
        $factor = round($factor, 2);
        return view('admin.index', [
            'chart'             => $chart->build(),
            'cortaChart'        => $cortaChart->build(),
            'fellowHostChart'   => $fellowHostChart->build(),
            'fellowHomeChart'   => $fellowHomeChart->build(),
            'pubPerYearChart'   => $pubPerYearChart->build(),
            'online'            => $online,
            'pub'               => $nbpub,
            'citations'         => $nbcitations,
            'auteurs'           => $total_auteur,
            'six'               => $six,
            'factor'               => $factor
        ]);
    } 
}
