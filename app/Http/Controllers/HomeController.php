<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Auteur;
use App\Models\Citation;
use App\Models\Funding;
use Illuminate\Http\Request;
use Spatie\SimpleExcel\SimpleExcelReader;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        return view('home');
    }
    public function import(Request $request)
    {
        $this->validate($request, [
            'fichier' => 'bail|required|file|mimes:xlsx'
        ]);

        //on déplace le fichier uploader vers le dossier "public" pour le lire

        $fichier = $request->fichier->move(public_path(), $request->fichier->hashName());

        //$reader: l'instance Spatie\SimpleExcel\SimpleExcelReader
        $reader = SimpleExcelReader::create($fichier);

        //On recupere le continu (les lignes) du fichier
        $rows = $reader->getRows();

        $datas = $rows->toArray();

        $id = 1; 
        foreach ($datas as $data){
            Auteur::create([
                'name'              => $data['Name'],
                'uniqueID'          => $data['Unique ID'],
                'currentCity'       => $data['Current City'],
                'currentCountry'    => $data['Current Country'],
                'scopusID'          => $data['Scopus ID'],
                'docMCperTenPubFel' => $data['Documents Most Contributed per 10 publications'],
                'PTNDocMCOfTheTPub' => $data['Proportion of total number of documents of Most Contributed of the total publica'],
                'TNDocMCPerFellow'  => $data['Total number of documents of Most Contributed per fellow'],
                'gender'            => $data['Gender'],
                'cohort'            => $data['Cohort'],
                'startYear'         => $data['Start year'],
                'nationality'       => $data['Nationality'],
                'homeInstitution'   => $data['Home institution'],
                'hostInstitution'   => $data['Host Institituion institution of registration'],
                'alternativeEmail'  => $data['Alternative e-mail'],
                'email'             => $data['Email'],
                'yearOfCompleting'  => $data['Year of completion'],
                'currentPHPStatus'  => $data['Current PhD Status '],
                'totalCo-authors'   => $data['Total Co-Authors'],
                'h-index'           => $data['h-index'],
                'numberOfCitation'  => $data['Number of Citations'],
                'numberOfPublication'=> $data['Number of Publication'],
                'scopusNumber'      => $data['ScopusNumber'],
                'nbCitBeforeCorta'  => $data['Number of Citations Before CARTA'],
                'nbCitDuringCorta'  => $data['Number of Citations During CARTA'],
                'nbCitAfterCorta'   => $data['Number of Citations After CARTA'],
                'mCTcombined1'      => $data['Most Contributed Topics Combined1'],
                'nbDocMC1'          => $data['Number of Documents of Most Contributed1'],
                'mCTcombined2'      => $data['Most Contributed Topics Combined2'],
                'nbDocMC2'          => $data['Number of Documents of Most Contributed2'],
                'mCTcombined3'      => $data['Most Contributed Topics Combined3'],
                'nbDocMC3'          => $data['Number of Documents of Most Contributed3']

            ]);

            $article = Article::create([
                'periodeOfPub'                  => $data['Article Publication Period: Before, During and After CARTA'],
                'JournalImpactFactor'           => $data['JournalImpactFactor'],
                'journalRank'                   => $data['JournalRank'],
                'numberOfAuthor'                => $data['Number of Authors'],
                'citedBy'                       => $data['Cited by'],
                'isbn'                          => $data['ISBN'],
                'issn'                          => $data['ISSN'],
                'publisher'                     => $data['Publisher'],
                'abstract'                      => $data['Abstract'],
                'link'                          => $data['DOI'],
                'doi'                           => $data['DOI'],
                'title'                         => $data['Title'],
                'sourceTile'                    => $data['Source title'],
                'cleanTitle'                    => $data['clean_title'],
                'publicationYear'               => $data['Publication Year'],
                'authorsID'                     => $data['Author(s) ID'],
                'authors'                       => $data['Authors'],
                'firstAuthorScopusID'           => $data['First Author Scopus ID'],
                'nbCortaCoauthPaper'            => $data['Number of CARTA Co-Authors on this paper'],
                'thePaperHasCortaFelAsCoauth'   => $data['The paper has CARTA fellows as Co-Authors'],
                'theCortaFelIsTheFirstAuthOfThis'=> $data['The CARTA fellow is the first author of this article']

            ]);
            

            Funding::create([
                'content'       => $data['Funding Text 1'],
                'article_id'    => $id
            ]);

            Funding::create([
                'content'       => $data['Funding Text 2'],
                'article_id'    => $id
            ]);

            Funding::create([
                'content'       => $data['Funding Text 3'],
                'article_id'    => $id
            ]);

            Funding::create([
                'content'       => $data['Funding Text 4'],
                'article_id'    => $id
            ]);

            Citation::create([
                'year'      => '2011',
                'number'    => $data['Citations in 2011'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2012',
                'number'    => $data['Citations in 2012'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2013',
                'number'    => $data['Citations in 2013'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2014',
                'number'    => $data['Citations in 2014'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2015',
                'number'    => $data['Citations in 2015'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2016',
                'number'    => $data['Citations in 2016'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2017',
                'number'    => $data['Citations in 2017'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2018',
                'number'    => $data['Citations in 2018'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2019',
                'number'    => $data['Citations in 2019'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2020',
                'number'    => $data['Citations in 2020'],
                'auteur_id'      => $id,
            ]);

            Citation::create([
                'year'      => '2021',
                'number'    => $data['Citations in 2021'],
                'auteur_id'      => $id,
            ]);
            $id++;
        }
        $reader->close();

        return view('home');


    }
}
