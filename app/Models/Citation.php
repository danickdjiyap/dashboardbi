<?php

namespace App\Models;

use App\Models\Auteur;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Citation extends Model
{
    use HasFactory;
    protected $fillable = [
        'year',
        'number',
        'auteur_id'
    ];

    public function auteur(){
    	return $this->belongsTo(Auteur::class);
    }
}
