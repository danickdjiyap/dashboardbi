<?php

namespace App\Models;

use App\Models\Citation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Auteur extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'uniqueID',
        'currentCity',
        'currentCountry',/*
        'nbPubBeforeCorta',
        'nbPubDuringCorta',
        'nbPubAfterCorta',*/
        'scopusID',
        'docMCperTenPubFel',
        'PTNDocMCOfTheTPub',
        'TNDocMCPerFellow',
        'gender',
        'cohort',
        'startYear',
        'nationality',
        'homeInstitution',
        'hostInstitution',
        'alternativeEmail',
        'email',
        'yearOfCompleting',
        'currentPHPStatus',
        'totalCo-authors',
        'h-index',
        'numberOfCitation',
        'numberOfPublication',
        'scopusNumber',
        'nbCitBeforeCorta',
        'nbCitDuringCorta',
        'nbCitAfterCorta',
        'mCTcombined1',
        'nbDocMC1',
        'mCTcombined2',
        'nbDocMC2',
        'mCTcombined3',
        'nbDocMC3'
    ];

    public function citations(){
        return $this->hasMany(Citation::class);
    }
}
