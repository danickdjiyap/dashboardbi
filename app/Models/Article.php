<?php

namespace App\Models;

use App\Models\Funding;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;
    protected $fillable = [
        'periodeOfPub',
        'JournalImpactFactor',
        'journalRank',
        'numberOfAuthor',
        'citedBy',
        'isbn',
        'issn',
        'publisher',
        'abstract',
        'link',
        'doi',
        'title',
        'sourceTile',
        'cleanTitle',
        'publicationYear',
        'authorsID',
        'authors',
        'firstAuthorScopusID',
        'nbCortaCoauthPaper',
        'thePaperHasCortaFelAsCoauth',
        'theCortaFelIsTheFirstAuthOfThis'
    ];

    public function funding(){
        return $this->hasMany(Funding::class);
    }
}
