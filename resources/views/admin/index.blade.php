@extends('layouts.master')

@section('contenulive')
<div class="text-bold p-3 text-xl">
    Bibliometrics Dashboard
</div>
<div class="row pt-2">
    <div class="col-lg-4">
        <div class="row">
            <div class="col-lg-6 col">
                <div class="small-box bg-white">
                    <div class="inner">
                        <h3>{{ $pub }}</h3>
                        <p>Number of publications</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer"><i class="fas fa-arrow-up"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col">
                <div class="small-box bg-white">
                    <div class="inner">
                        <h3>{{ $online }}</h3>
                        <p>Number of online version</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer"><i class="fas fa-arrow-up"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col">
                <div class="small-box  bg-white">
                    <div class="inner">
                        <h3>{{ $auteurs }}</h3>
                        <p>Total auteurs</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer"><i class="fas fa-arrow-up"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col">
                <div class="small-box bg-white">
                    <div class="inner">
                        <h3>{{ $citations }}</h3>
                        <p>Total citations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer"> <i class="fas fa-arrow-up"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col">
                <div class="small-box  bg-white">
                    <div class="inner">
                        <h3>{{ $factor }}</h3>
                        <p>Medias impact factor of journals</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer"> <i class="fas fa-arrow-up"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col">
                <div class="small-box bg-white">
                    <div class="inner">
                        <h3>{{ $six }}<sup style="font-size: 20px">%</sup></h3>
                        <p>% Papers withs fellows as 1st author</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">{{ $six }} % <i class="fas fa-arrow-up"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="container mx-auto">

            <div class=" bg-white rounded shadow">
                {!! $pubPerYearChart->container() !!}
            </div>
        
        </div>
        
        <script src="{{ $pubPerYearChart->cdn() }}"></script>
        
        {{ $pubPerYearChart->script() }}
    </div>
</div>


<div class="row pt-3">
    <div class="col-lg-4">
        <div class="row">
            <div class="col-lg-12 col">
                <div class="small-box bg-white">
                    <div class="inner">
                        <h3>{{ $online }}</h3>
                        <p>Number of online version</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer"> <i class="fas fa-arrow-up"></i></a>
                </div>
            </div>
            <div class="col-lg-12 col">
                <div class="container mx-auto">

                    <div class=" bg-white rounded shadow">
                        {!! $cortaChart->container() !!}
                    </div>
                
                </div>
                
                <script src="{{ $cortaChart->cdn() }}"></script>
                
                {{ $cortaChart->script() }}
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="container mx-auto">

            <div class=" bg-white rounded shadow">
                {!! $chart->container() !!}
            </div>
        
        </div>
        
        <script src="{{ $chart->cdn() }}"></script>
        
        {{ $chart->script() }}
    </div>
</div>

<div class="row my-5 pb-3">
    <div class="col-lg-6">
        <div class="container mx-auto">

            <div class=" bg-white rounded shadow">
                {!! $fellowHostChart->container() !!}
            </div>
        
        </div>
        
        <script src="{{ $fellowHostChart->cdn() }}"></script>
        
        {{ $fellowHostChart->script() }}
    </div>
    <div class="col-lg-6">
        <div class="container mx-auto">

            <div class=" bg-white rounded shadow">
                {!! $fellowHomeChart->container() !!}
            </div>
        
        </div>
        
        <script src="{{ $fellowHomeChart->cdn() }}"></script>
        
        {{ $fellowHomeChart->script() }}
    </div>
</div>
@endsection