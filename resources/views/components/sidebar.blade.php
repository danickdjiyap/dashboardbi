<aside class=" active control-sidebar-light" style="display:block mt--3 pr-2">
    <!-- Control sidebar content goes here -->
    <div class="bg-light">
      <div class="card-body box-profile bg-white">
        <div class="text-center">
          <img src="{{asset('images/male.png')}}" class="profile-user-img img-fluid img-circle" alt="User Image">
        </div>

        <h3 class="profile-username text-center ellipsis">Danick Tchakouani</h3>

        <p class="text-muted text-center">contact@dalalcode.com</p>

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Accueil
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Tableau de bord
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          </li>
        </ul>
      </div>
      <!-- /.card-body -->
    </div>
  </aside>